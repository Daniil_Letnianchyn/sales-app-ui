import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import * as components from './components';

const routes: Routes = [
  { path: 'app-customers', component: components.CustomersComponent},
  { path: 'app-employees', component: components.EmployeesComponent},
  { path: 'app-products', component: components.ProductsComponent},
  { path: 'app-sales', component: components.SalesComponent},
  { path: 'app-sales/:saleId', component: components.SaleDetailsComponent},
  { path: 'app-sale-add', component: components.SaleAddComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
