import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import * as components from './components';
import { HttpClientModule } from '@angular/common/http';
import { SaleService } from './shared/services/sale.service';
import { EmployeeService } from './shared/services/employee.service';
import { CustomerService } from './shared/services/customer.service';
import { ProductService } from './shared/services/product.service';

@NgModule({
  declarations: [
    AppComponent,
    components.CustomersComponent,
    components.EmployeesComponent,
    components.ProductsComponent,
    components.SaleAddComponent,
    components.SaleDetailsComponent,
    components.SalesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    SaleService,
    EmployeeService,
    CustomerService,
    ProductService,
    SaleService
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {
  constructor(){}

}
