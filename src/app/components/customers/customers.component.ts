import { Component, OnInit } from "@angular/core";
import { Customer } from "../../shared/models";
import { CustomerService } from 'src/app/shared/services/customer.service';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit{

  public customers: Customer[] = [];

  constructor(private customerService: CustomerService) {
  }

  ngOnInit(): void {
    this.customerService.getCustomers()
    .subscribe(customers => {
        this.customers = customers;
    });
  }

}
