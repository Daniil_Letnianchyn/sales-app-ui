
export { SalesComponent } from './sales/sales.component';
export { SaleDetailsComponent } from './sales/sale-details/sale-details.component'
export { SaleAddComponent } from './sales/sale-add/sale-add.component'
export { ProductsComponent } from './products/products.component'
export { EmployeesComponent } from './employees/employees.component'
export { CustomersComponent } from './customers/customers.component'
