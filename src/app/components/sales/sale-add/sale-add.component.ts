import { Component, OnInit, OnDestroy } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Customer, Employee, Product, SaleForCreation } from 'src/app/shared/models';
import { CustomerService } from 'src/app/shared/services/customer.service';
import { EmployeeService } from 'src/app/shared/services/employee.service';
import { Router } from '@angular/router';
import { ProductService } from 'src/app/shared/services/product.service';
import { SaleService } from 'src/app/shared/services';

@Component({
  selector: 'app-sale-add',
  templateUrl: './sale-add.component.html',
  styleUrls: ['./sale-add.component.css']
})
export class SaleAddComponent implements OnInit, OnDestroy{

  public saleForm: FormGroup;

  public customers: Customer[];
  public employees: Employee[];
  public products: Product[];


  constructor(private customerService: CustomerService,
    private employeeService: EmployeeService,
    private productService: ProductService,
    private saleService: SaleService,
    private formBuilder: FormBuilder,
    private router: Router,) { }

    ngOnInit(): void {

      this.saleForm = this.formBuilder.group({
        customer: ['', [Validators.required]],
        employee: ['', [Validators.required]],
        product: ['', [Validators.required]],
        quantity: [0, [Validators.required, Validators.max(99999), Validators.min(1)]]
      });

      this.customerService.getCustomers()
      .subscribe(result => {
        this.customers = result;
      });
      this.employeeService.getEmployees()
      .subscribe(result => {
        this.employees = result;
      });
      this.productService.getProducts()
      .subscribe(result => {
        this.products = result;
      });
    }


    ngOnDestroy(): void {
    }

    addSale(): void {
      if (this.saleForm.dirty && this.saleForm.valid) {

          var saleForCreation: SaleForCreation = {
            CustomerId: this.saleForm.controls.customer.value,
            EmployeeId: this.saleForm.controls.employee.value,
            ProductId: this.saleForm.controls.product.value,
            Quantity: this.saleForm.controls.quantity.value
          }

          this.saleService.addSale(saleForCreation)
          .subscribe(
            () => {
              this.router.navigateByUrl('/app-sales');
            });
      }
    }
}
