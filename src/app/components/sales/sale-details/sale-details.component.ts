import { Component, OnInit, OnDestroy } from "@angular/core";
import { Subscription } from "rxjs";
import { ActivatedRoute } from "@angular/router";
import { Sale } from "../../../shared/models";
import { SaleService } from "../../../shared/services";

@Component({
  selector: 'app-sale-details',
  templateUrl: './sale-details.component.html',
  styleUrls: ['./sale-details.component.css']
})
export class SaleDetailsComponent implements OnInit, OnDestroy{

  public sale: Sale;

  private sub: Subscription;
  public saleId: string;

  constructor(private saleService: SaleService,
    private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(
      params => {
        this.saleId = params['saleId'];
        this.saleService.getSaleById(this.saleId)
        .subscribe(result => {
          this.sale = result;
        });
      }
    );
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

}
