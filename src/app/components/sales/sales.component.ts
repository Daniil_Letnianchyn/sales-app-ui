import { Component, OnInit } from "@angular/core";
import { Sale } from 'src/app/shared/models';
import { SaleService } from 'src/app/shared/services';

@Component({
  selector: 'app-sales',
  templateUrl: './sales.component.html',
  styleUrls: ['./sales.component.css']
})
export class SalesComponent implements OnInit{

  public sales: Sale[] = [];

  constructor(private saleService: SaleService) {
  }

  ngOnInit(): void {
    this.saleService.getSales()
    .subscribe(sales => {
        this.sales = sales;
    });
  }

}
