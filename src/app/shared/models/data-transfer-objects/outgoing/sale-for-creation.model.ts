export class SaleForCreation{
  CustomerId: string;
  EmployeeId: string;
  ProductId: string;
  Quantity: number;
}
