export class Sale{
  id: string;
  date: Date;
  customer: string;
  employee: string;
  product: string;
  unitPrice: number;
  quantity: number;
  totalPrice: number;
}
