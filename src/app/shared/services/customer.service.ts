import { Injectable } from "@angular/core";
import { BaseService } from "./base.service";
import { HttpClient } from "@angular/common/http";
import { Customer } from "../models";
import { Observable } from "rxjs";

@Injectable()
export class CustomerService extends BaseService {
  constructor(private http: HttpClient) {
    super();
  }

  getCustomers(): Observable<Customer[]> {
    return this.http.get<Customer[]>(`${this.apiUrl}/customers`,
    { headers: { 'Content-Type': 'application/json' } });
  }

}
