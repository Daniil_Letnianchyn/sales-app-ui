import { Injectable } from "@angular/core";
import { BaseService } from "./base.service";
import { HttpClient } from "@angular/common/http";
import { Employee } from "../models";
import { Observable } from "rxjs";

@Injectable()
export class EmployeeService extends BaseService {
  constructor(private http: HttpClient) {
    super();
  }

  getEmployees(): Observable<Employee[]> {
    return this.http.get<Employee[]>(`${this.apiUrl}/employees`,
    { headers: { 'Content-Type': 'application/json' } });
  }

}
