import { Injectable } from "@angular/core";
import { BaseService } from "./base.service";
import { HttpClient } from "@angular/common/http";
import { Product } from "../models";
import { Observable } from "rxjs";

@Injectable()
export class ProductService extends BaseService {
  constructor(private http: HttpClient) {
    super();
  }

  getProducts(): Observable<Product[]> {
    return this.http.get<Product[]>(`${this.apiUrl}/products`,
    { headers: { 'Content-Type': 'application/json' } });
  }

}
