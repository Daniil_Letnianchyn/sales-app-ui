import { Injectable } from "@angular/core";
import { BaseService } from "./base.service";
import { HttpClient } from "@angular/common/http";
import { Sale, SaleForCreation } from "../models";
import { Observable } from "rxjs";

@Injectable()
export class SaleService extends BaseService {
  constructor(private http: HttpClient) {
    super();
  }

  getSales(): Observable<Sale[]> {
    return this.http.get<Sale[]>(`${this.apiUrl}/sales`,
    { headers: { 'Content-Type': 'application/json' } });
  }

  getSaleById(saleId: string): Observable<Sale> {
    return this.http.get<Sale>(`${this.apiUrl}/sales/${saleId}`,
    { headers: { 'Content-Type': 'application/json' } });
  }

  addSale(saleToAdd: SaleForCreation): Observable<Sale> {
    return this.http.post<Sale>(`${this.apiUrl}/sales`, saleToAdd,
    { headers: { 'Content-Type': 'application/json' } });
  }

}
