export const environment = {
  production: true,
  apiUrl: 'http://localhost:49591/api',
};
// N-Tier                    http://localhost:63371/api
// Domain-centric            http://localhost:52190/api
// Application Layer         http://localhost:55218/api
// SQRS                      http://localhost:49591/api
